# Quiz 5 Group 4
## 1. What is the output of this code?
```
f x = [reverse [1..y] \\ y <- [1..x]]
Start = f 3
```
a. [[1,2,3],[1,2],[1]]\
b. [[3,2,1],[2,1],[1]]\
c. [[1],[1,2],[1,2,3]]\
d. [[1],[2,1],[3,2,1]]\
Correct answer: D

## 2. What is the output of this code?
```
Start = fst (1,2,3) + fst (2,5) + snd (3,2)
```
a. [3,12]\
b. Type error\
c. [1,4]\
d. Run time error\
Correct answer: B

## 3. What is the output of `splitAt 3 ['EvanIsTheBestTeacher']`?

a. (['E','v','a','n'],['I','s','T','h','e','B','e','s','t','T','e','a','c','h','e','r'])\
b. [['E','v','a','n'],['I','s','T','h','e','B','e','s','t','T','e','a','c','h','e','r']]\
c. (['E','v','a'],['n','I','s','T','h','e','B','e','s','t','T','e','a','c','h','e','r'])\
d. [['E','v','a'],['n','I','s','T','h','e','B','e','s','t','T','e','a','c','h','e','r']]\
Correct answer: C

## 4. In a function signature `qsort :: [t] -> [t] | Ord t`, what does `Ord t` mean?

a. Order is defined for variable t\
b. Order is defined for type t\
c. t is an Ordinary variable\
d. t is an Ordinary type\
Correct answer: B

## 5. Which statement is wrong?
a. Lists are written using [] and tuples are written using ()\
b. Tuples can have elements of different types, while type of the list elements must be the same\
c. Length of tuples must be fixed\
d. We can use pattern matching for lists, but not for tuples\
Correct answer: D
