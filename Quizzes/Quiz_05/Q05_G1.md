# Quiz 5 Group 1
## 1. What is the output of this code?
```
Start = [ ( x , y) \\ x <- [1..5] & y <- [1..x] | isOdd x ]
```
a. [(1,1),(3,3),(5,5)]\
b. []\
c. [(1,1),(3,1),(3,2),(3,3),(5,1),(5,2),(5,3),(5,4),(5,5)]\
d. Compile Error\
Correct answer: D

## 2. What is the output of this code?
```
Start = zip ([1 ,2 ,3], [])
```
a. []\
b. Type error\
c. [1,2,3]\
d. Run time error\
Correct answer: A

## 3. What is the output of this code?
```
Start = [ (snd x)^2 \\ x <- [(1,2),(2, 3),(3,4),(4,5) ] | isEven(fst x) ]
```
a. []\
b. [9,25]\
c. [4,16]\
d. none of above\
Correct answer: B


## 4.What is the output of this code?
```
Start = snd (['a','b','c'],(6,9))
```
a. 9\
b. (6,9)\
c. 'b'\
d. none of above\
Correct answer: b

## 5. What is the output of the following code?
```
Start = fst(splitAt 3 [6,9]) ++ snd (splitAt 1 [1,2])
```
a. [6,9,2]\
b. [6,2]\
c. [2]\
d. None of above\
Correct answer: A
